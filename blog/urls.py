from rest_framework.routers import SimpleRouter
from django.urls import path, include
from blog.views import PostModelViewSet

router = SimpleRouter()
router.register('posts', PostModelViewSet)

urlpatterns = [
    path('', include(router.urls))
]
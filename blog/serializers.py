from rest_framework.serializers import ModelSerializer, StringRelatedField
from blog.models import Post


class PostSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'


class PostListSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = ('id', 'title', 'category')


class PostDetailSerializer(ModelSerializer):
    tag = StringRelatedField(read_only=True, many=True)
    category = StringRelatedField()

    class Meta:
        model = Post
        fields = '__all__'
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from blog.models import Post
from blog.serializers import PostSerializer, PostListSerializer, PostDetailSerializer
from rest_framework.permissions import AllowAny
from rest_framework.permissions import IsAdminUser


class PostModelViewSet(ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [AllowAny, ]

    def get_serializer_class(self):
        if self.action == "list":
            return PostListSerializer
        elif self.action == "retrieve":
            return PostDetailSerializer
        return PostSerializer
    
    def get_permissions(self):
        if self.action == "list" or self.action == "retrieve":
            permission_classes = [AllowAny, ]
        else:
            permission_classes = [IsAdminUser, ]
        return [permission() for permission in permission_classes]
